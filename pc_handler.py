import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado import gen
from models import AsyncUser


class Application(tornado.web.Application):
	def __init__(self):
		handlers = [
			(r"/", UserHandler),
		]
		tornado.web.Application.__init__(self, handlers)

class UserHandler(tornado.web.RequestHandler):

	def set_default_headers(self):
		self.set_header("Access-Control-Allow-Origin", "*")
		self.set_header("Access-Control-Allow-Headers", "*")
		self.set_header("Access-Control-Allow-Headers","accept, cache-control, origin, x-requested-with, x-file-name, content-type")

	@gen.coroutine
	def get(self):
		try:
			url = self.get_argument("url")
		except Exception as e:
			print(e)
			self.write("")
		user = AsyncUser()
		if url != None:
			r = yield (user.site_check(url))
			self.write(r)
		else:
			self.write("")
		self.finish()


def main():
	try:
		http_server = tornado.httpserver.HTTPServer(Application())
		PORT = 8001
		print("Server bound: ", PORT)
		http_server.listen(PORT)
		tornado.ioloop.IOLoop.instance().start()
	except Exception as e:
		print(e)

if __name__ == "__main__":
	main()