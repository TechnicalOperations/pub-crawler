# PubCrawler README

Website information tool.  
Overall Purpose:
```
  * Reduce the amount of time and resources given by PubOps during the onboarding process
  * Verify that the publisher being onboarded will be a profitable decision
  * Improve onboard targetting of profitable publishers
  * Combine logic from existing TechOps tools into a single resource
```

Will Provide:
```
  * Determine the publisher from website hostname alone
  * Run Pixalate/Grapeshot scoring to determine traffic's RX DSP tier group
  * Determine inventory availability per site
  * Determine IAB categorization and website language
  * Determine any active ad partner on website
  * Determine valid ad placements and sizes avaiable on site
  * Record common IVT found per site per percentage
  * Social media information
```