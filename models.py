from tornado.concurrent import return_future
from pc_app import App

class AsyncUser(object):
	@return_future
	def site_check(self, url, callback=None):
		app = App()
		result = app.main(url)
		callback(result)
