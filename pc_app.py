import sys
import os

sys.path.append(os.path.join(os.path.dirname(__file__), "./subdomains"))
sys.path.append(os.path.join(os.path.dirname(__file__), "./crawler"))
sys.path.append(os.path.join(os.path.dirname(__file__), "./social"))

from reqlib import Domain
from crawl import Crawler
from sm_info import SocialMedia
from pixalate import Pixalate
from wholookup import Who
import json
import datetime
from threading import Thread
import queue
import threading
import ast


class App:

	def main(self,url):

		# Get timestamp
		def get_date():
			timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
			return timestamp


		# Get subdomain info
		def get_domain(q1, url):
			d = Domain()
			sd = d.find_subdomains(str(url))
			q1.put(sd)
			return


		# Get crawler results
		def get_sc(q2, url):
			sc_crw = Crawler()
			sc_url = sc_crw.crawl(str(url))
			q2.put(sc_url)
			return


		# Get social media info
		def get_sm(q3, sc_url):
			sm = SocialMedia()
			# sm_fb = sm.facebook_info(sc_url[0])
			# print(sm_fb)
			sm_tw = sm.twitter_info(sc_url[1])
			print(sm_tw)
			a = []
			# a.append(sm_fb)
			a.append(sm_tw)
			q3.put(a)
			return


		# Get Pixalate info
		def get_rating(q4, url):
			px = Pixalate()
			px_rate = px.source_rating(str(url))
			q4.put(px_rate)
			return


		# Get WHOIS record
		def get_whois(q5, url):
			wi = Who()
			wl = wi.lookup(str(url))
			q5.put(wl)
			return


		timestamp = get_date()

		q1 = queue.Queue()
		q4 = queue.Queue()
		q5 = queue.Queue()


		t1 = Thread(target=get_domain, args=(q1, url,))
		# t2 = Thread(target=get_sc, args=(q2,url,))
		t4 = Thread(target=get_rating, args=(q4, url,))
		t5 = Thread(target=get_whois, args=(q5, url))

		print("[+]Firing subdomain lookup (thread)")
		t1.start()
		# print("[+]Firing website crawler (thread)")
		# t2.start()
		print("[+]Firing IVT lookup (thread)")
		t4.start()
		print("[+]Firing WHOIS lookup (thread)")
		t5.start()

		t1.join()
		print("\t[-]get_domain returned")
		t4.join()
		print("\t[-]get_rating returned")
		t5.join()
		print("\t[-]get_whois returned")

		sd = q1.get()
		p_rating = q4.get()
		if p_rating == 1:
			return ""
		wl = q5.get()

		if sd != None and p_rating != None and wl != None:
			# Domain and Time
			resp = "{"
			resp = resp + '"domain":"{}",'.format(url)
			resp = resp + '"timestamp":"{}",'.format(timestamp)
			resp = resp + '"publisher":"{}",'.format(wl)

			# Facebook


			# Twitter


			# SubDomains
			resp = resp + '"subdomains": ['
			for i in sd:
				resp = resp + "{"
				resp = resp + '"host":"{}",'.format(i[0])
				resp = resp + '"ip":"{}",'.format(i[1])
				resp = resp + '"city":"{}",'.format(i[2])
				resp = resp + '"loc":"{}"'.format(i[3])
				if sd.index(i) < (len(sd) - 1):
					resp = resp + "},"
				else:
					resp = resp + "}"
			resp = resp + '],'

			resp = resp + p_rating
			# End Bracket
			resp = resp + "}"

			# Send back to XHR
			return resp
		else:
			return ""



