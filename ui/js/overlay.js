// Loading screen overlay
function overlay () {
	// Show full page LoadingOverlay
	$.LoadingOverlaySetup({
	    color           : "rgba(0, 0, 0, 1)",
	    image           : "img/loading.gif",
	    maxSize         : "1500px",
	    minSize         : "200px",
	    resizeInterval  : 1,
	    size            : "150%"
	});
	$.LoadingOverlay("show");

	// Hide it after 15 seconds
	setTimeout(function(){
		$.LoadingOverlay("hide");
	}, 15000);
}
