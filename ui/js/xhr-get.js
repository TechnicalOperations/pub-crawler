// AJAX request to pull JSON
// Must be called as follows:
// 1. Initialize XHR object
// 2. XHR Open
// 3. OnReadyStateChange conditional(s)
// 4. XHR request send
function XMLGET (domain) {
	// Call loading overlay
	overlay();

	// Reset search field defaults
	document.getElementById('domain').value = "";
	document.getElementById('domain').setAttribute('placeholder', 'search...');

	// AJAX request
	console.log("[+]Creating XHR object: ");
	var xhr = new XMLHttpRequest();
	console.log("\t" + xhr);
	// Loopback IP for testing only
	xhr.open("GET","http://10.7.1.74:5000/?url=" + domain, true);
	xhr.timeout = 20000;
	console.log("[+]Timeout: " + xhr.timeout);
	xhr.onreadystatechange = function() {
        	if(xhr.readyState==4 && xhr.status==200) {
        	    console.log(xhr.response);
        	}
	}
	xhr.send();

	// Prevent refresh of page on submit
	return false;
}
