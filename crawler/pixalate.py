import json
from pprint import pprint
import requests
import argparse
import ast

class Pixalate:

    def source_rating(self,site):

        urls = [
            "https://idp.pixalate.com/services/2001/Realm/login?username=ro&rememberMe=false&password=RadRhythm1!&callback=__ng_jsonp__.__req24.finished",
            "https://ratings2-srvc.pixalate.com/services/2012/Report/getWidget?q=WHERE%20adDomain=%27{}%27%20AND%20countryCode=%27US%27%20%20AND%20deviceType%20=%20%27desktop%27%20%20&reportId=mrt&widgetId=domainSummary".format(site)
        ]
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
        }

        r = requests.session()
        try:
            req = r.get(urls[0], headers=headers)
        except Exception as e:
            print(e)
            return 1

        try:
            req2 = r.get(urls[1], headers=headers)
        except Exception as e:
            print(e)
            return 1

        data = json.loads(req2.text)

        try:
            cat = "'domain_category': '" + str(data['overview']['domainCategory'].replace("_", " ")) +"',"
            sub_cat = "'domain_subcategory': '" + str(data['overview']['domainSubCategory'].replace("_", " ")) +"',"

            fraud_grade = "'risk': '" + str(data['overview']['pixalateGrade']) +"',"
            fraud_lvl = "'risktype': '" + str(data['overview']['domainFraudType'].replace("_", " ")) +"',"
            fraud_type = "'risk_info': " + str(data['ivtDistribution']['givtFraudTypes']) +","

            inv_dist_us = "'inv_nonus': " + str(data['inventoryDistribution']['countries'][2]) + ','
            inv_dist_non = "'inv_us': " + str(data['inventoryDistribution']['countries'][1]) + ','
            adsize = '"adsizes": ' + str(data['inventoryDistribution']['adSizes']) + ','

            prog_inv = '"prog_inv": ' + str(data['programmaticInventory']['months']) + ','

            device_dist = '"devices": ['
            for i in data['deviceDistribution']['deviceTypes']:
                device_dist += str(i) + ","
            device_dist = device_dist[:-1]
            device_dist += "]"

            p_dict = cat + sub_cat + fraud_grade + fraud_lvl + fraud_type + inv_dist_us + inv_dist_non + adsize + prog_inv + device_dist
            p_dict = p_dict.replace("'",'"')

        except Exception as e:
            return 1

        return p_dict
