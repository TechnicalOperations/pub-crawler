import sys
import os

import re

sys.path.append(os.path.join(os.path.dirname(__file__), "../util"))
from ua_gen import RandomUA
from pyvirtualdisplay import Display
from selenium import webdriver
from bs4 import BeautifulSoup


class Crawler:

	def crawl(self, host):

		def scrape(html):
			link = []
			data = BeautifulSoup(html, 'html.parser')
			for a in data.find_all('a', href=True):
				link.append(a['href'])
			return link


		display = Display(visible=0, size=(800, 1000))
		display.start()
		rua = RandomUA()
		ua = rua.ua_gen()

		caps = webdriver.DesiredCapabilities.FIREFOX.copy()
		caps['webdriver_accept_untrusted_certs'] = True
		caps['webdriver_assume_untrusted_issuer'] = True

		profile = webdriver.FirefoxProfile()
		profile.set_preference("general.useragent.override", ua)

		driver = webdriver.Firefox(profile)
		driver.set_page_load_timeout(30)

		fqdn = "http://{}".format(host)

		try:
			driver.get(fqdn)
			src = driver.page_source
		except Exception as e:
			src = driver.page_source
			pass

		links = scrape(src)
		fb = ""
		tw = ""
		social = []
		for l in links:
			fb_m = re.search("^.+(facebook.com.+)$", l)
			tw_m = re.search("^.+(twitter.com.+)$", l)
			if fb_m:
				fb = "{}".format(fb_m.group(1))
				social.append(fb)
			if tw_m:
				tw = "{}".format(tw_m.group(1))
				social.append(tw)
		return social
