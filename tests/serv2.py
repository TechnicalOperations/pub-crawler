from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "./subdomains"))
sys.path.append(os.path.join(os.path.dirname(__file__), "./crawler"))
from reqlib import Domain
from crawl import Crawler
import threading
import cgi
import json
import datetime
from threading import Thread
import queue

class Handler(BaseHTTPRequestHandler):

    def do_GET(self):

        def parse_url(path):
            url = ""
            idx = path.find('?')
            if idx >= 0:
                rpath = path
                rpath = rpath[2:]
                rpath = rpath.split("&")
                for i in rpath:
                    if i.startswith("url=") and i[4:] is not None:
                        url = i[4:]

            return url

        def get_date():
            timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
            return timestamp

        def get_domain(q1, url):
            d = Domain()
            sd = d.find_subdomains(str(url))
            q1.put(sd)
            return

        def get_fb(q2, url):
            fb_crw = Crawler()
            facebook_url = fb_crw.crawl(str(url))
            q2.put(facebook_url)
            return

        timestamp = get_date()
        url = parse_url(self.path)
        self.send_response(200)
        self.end_headers()


        q1 = queue.Queue() # Domain
        q2 = queue.Queue() # Facebook

        t1 = Thread(target=get_domain, args=(q1,url,))
        t2 = Thread(target=get_fb, args=(q2,url,))
        t1.start()
        t2.start()
        t1.join()
        t2.join()

        sd = q1.get()
        facebook_url = q2.get()

        resp = "{"
        resp = resp + "'domain':'{}',".format(url)
        resp = resp + "'timestamp':'{}',".format(timestamp)
        #resp = resp + "'facebook':[{'location':'{}'}],".format(facebook_url)
        resp = resp + "'subdomains':{}".format(sd)
        resp = resp + "}"

        #x = {'name':'Michael','url':str(url)}
        #message = json.dumps(x)
        self.wfile.write(bytes(resp, "utf8"))


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""
    pass

if __name__ == '__main__':
    server = ThreadedHTTPServer(('127.0.0.1', 8081), Handler)
    print('Starting server, use <Ctrl-C> to stop')
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print("\nExit received")
        print("Gracefully shutting down")
        server.server_close()
