from flask import Flask
from flask import request
import datetime
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "./subdomains"))
sys.path.append(os.path.join(os.path.dirname(__file__), "./crawler"))
from reqlib import Domain
from crawl import Crawler
import json
import datetime
from threading import Thread
from queue import *
import threading

app = Flask(__name__)

def get_date():
    timestamp = '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
    return timestamp

def get_domain(q1, url):
    d = Domain()
    sd = d.find_subdomains(str(url))
    q1.put(sd)
    return

def get_fb(q2, url):
    fb_crw = Crawler()
    facebook_url = fb_crw.crawl(str(url))
    q2.put(facebook_url)
    return


@app.route("/", methods=['GET'])
def main():
    url = request.args.get('url')
    timestamp = get_date()

    q1 = queue.Queue()
    q2 = queue.Queue() # Facebook

    t1 = Thread(target=get_domain, args=(q1,url,))
    t2 = Thread(target=get_fb, args=(q2,url,))
    t1.start()
    t2.start()
    t1.join()
    t2.join()

    sd = q1.get()
    facebook_url = q2.get()

    resp = "{"
    resp = resp + '"domain":"{}",'.format(url)
    resp = resp + '"timestamp":"{}",'.format(timestamp)
    #resp = resp + '"facebook":[{"location":"{}"}],'.format(facebook_url)
    resp = resp + '"subdomains":{}'.format(sd)
    resp = resp + "}"

    return resp
