import requests
from bs4 import BeautifulSoup

headers = {
	'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'
}

url = "https://www.similarweb.com/website/bornrich.com"

r = requests.session()
req = r.get(url, headers=headers)
print(req.text)
html = BeautifulSoup(req.text, 'html.parser')
rank = ""

for i in html.findAll("div", {"data-rank-subject":"Global"}):
	for v in i.findAll("span", {"class":"rankingItem-value js-countable"}):
		print(v)
		rank = v['data-value']

rank = rank.replace(",","")
print("Rank: {}".format(rank))


