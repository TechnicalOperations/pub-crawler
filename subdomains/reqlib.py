import re
import requests
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "../util"))
from ua_gen import RandomUA
from iplookup import CheckIP
from bs4 import BeautifulSoup
import json
import twitter


class Domain:

    def find_subdomains(self,host):

        # HTTP/S Request
        def get_req(host):
            # List to hold results
            j = []

            # Pull User-Agent String
            rua = RandomUA()
            ua = rua.ua_gen()

            # Disregard SSL Warnings
            requests.packages.urllib3.disable_warnings()

            # Issue HTTP request
            try:
                headers = {'user-agent': ua,
                           'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                           'accept-language': 'en-US,en;q=0.8',
                           'referer': 'https://www.bing.com/',
                           'cache-control': 'max-age=0',
                           'upgrade-insecure-requests': '1'
                }
                req = requests.get(host, headers=headers, allow_redirects=True, timeout=3)
                if req.status_code == 200:
                    soup = BeautifulSoup(req.text, "html.parser")
                    for i in soup.find_all("cite"):
                        text = i.text
                        if "http://" in text:
                            text = text[7:]
                        if "https://" in i.text:
                            text = text[8:]
                        patt = ["^(\w+\.\w+)$",
                                "(^\w+\.\w+\.\w+)$",
                                "(^\w+\.\w+)\/",
                               ]
                        for p in patt:
                            m = re.search(p,text)
                            if m:
                                j.append(m.group(1))
            except:
                pass
            return j

        s_entry = 1
        subdomains = []
        while s_entry < 52:
            x = get_req("http://www.bing.com/search?q=site%3A{0}&first={1}".format(host,str(s_entry)))
            for i in x:
                subdomains.append(i)
            s_entry += 10

        subdomains = set(subdomains)
        i = CheckIP()
        ips = i.resolve(subdomains)

        return ips
