import socket
from multiprocessing.dummy import Pool as ThreadPool
import requests
import ast
import json

class CheckIP:

    def resolve(self,a):

        def lookup(host):
            try:
                ip = socket.gethostbyname(host)
                req = requests.get("http://ipinfo.io/{}".format(ip), timeout=5)
                info = req.text
                info = info.replace("\n", "")
                #info = ast.literal_eval(info)
                info = json.loads(info)

                city = info['city']
                loc = info['loc']

                #print(info)
                #info = str(info)
                #info = info.replace("'", '"')

                x = []
                x.append(host)
                x.append(ip)
                x.append(city)
                x.append(loc)
                return x
            except:
                return None

        pool = ThreadPool(30)
        results = pool.map(lookup, a)
        results = filter(None, results)
        results = list(results)
        print(results)
        return results
